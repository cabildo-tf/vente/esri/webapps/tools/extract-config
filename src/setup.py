import setuptools

setuptools.setup(
    name="extract_config",
    version="0.0.4",
    author="Ignacio Lorenzo",
    author_email="nacholore@gmail.com",
    description="Extract config of ArcGIS Web AppBuilder",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
          'pyyaml'
      ],
    entry_points={
        'console_scripts': ['extract-config=extract_config.extract_config:main'],
    }
)
