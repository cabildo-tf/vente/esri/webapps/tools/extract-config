import unittest

from extract_config.extract_config import ProcessConfig, load_config
from nose.tools import eq_


class TestExtracConfig(unittest.TestCase):
    def test_replaceConfig_when_passReplaceConfig(self):
        config_path = './test/resources/config.json'
        config_min_path = './test/resources/config-replace.min.json'
        replaces_path = './test/resources/replace.yaml'
        process = ProcessConfig(config_path=config_path, replaces_path=replaces_path)
        config = process.run()
        config_expected = load_config(config_min_path)
        self.maxDiff = None
        self.assertDictEqual(config, config_expected)

    def test_replaceConfig_when_withoutReplaceConfig(self):
        config_path = './test/resources/config.json'
        config_min_path = './test/resources/config.min.json'
        process = ProcessConfig(config_path=config_path)
        config = process.run()
        config_expected = load_config(config_min_path)
        eq_(config, config_expected)

    def test_replaceArray_when_passReplaceArrayConfig(self):
        config_path = './test/resources/config.json'
        config_min_path = './test/resources/config.min.json'
        process = ProcessConfig(config_path=config_path)
        config = process.run()
        config_expected = load_config(config_min_path)
        eq_(config, config_expected)
