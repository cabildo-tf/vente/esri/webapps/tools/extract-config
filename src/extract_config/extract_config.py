import argparse
import json
import os
import re
import yaml


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--input-config", dest="input_config_path", default="./config.json")
    parser.add_argument("-o", "--output-config", dest="output_config_path", default="./config-replace.min.json")
    parser.add_argument("-r", "--replace-config", dest="replace_config_path")
    args = parser.parse_args()
    process = ProcessConfig(config_path=args.input_config_path, replaces_path=args.replace_config_path)
    config = process.run()
    save_config(config, args.output_config_path)


def load_config(path):
    if path and os.path.exists(path):
        with open(path, 'r') as f:
            if path.lower().endswith('.json'):
                return json.load(f)
            elif path.lower().endswith(('.yaml', '.yml')):
                return yaml.safe_load(f.read())


class ProcessConfig(object):
    def __init__(self, config_path, *args, **kwargs):
        self.__config = load_config(config_path)
        self.__root_path = os.path.dirname(config_path)
        replaces_path = kwargs.pop('replaces_path', None)
        self.__replaces = load_config(replaces_path) if replaces_path else None
        self.__separator = kwargs.pop('separator', ':')
        self.__root = kwargs.pop('root', 'R')

    def run(self):
        return self.__process(self.__config, self.__root)

    def __process(self, data, path):
        if isinstance(data, dict):
            for k, v in data.items():
                if k == 'config' and isinstance(v, str):
                    v = load_config(os.path.join(self.__root_path, v))
                data[k] = self.__process(v, path + self.__separator + k)
            return data
        elif isinstance(data, list):
            data = self.__operations(data, path + "[]")
            if isinstance(data, list):
                return [self.__process(v, path + "[" + str(i) + "]") for i, v in enumerate(data, start=1)]
            else:
                return data
        else:
            return self.__operations(data, path)

    def __operations(self, data, path):
        if self.__replaces:
            for k, v in self.__replaces.items():
                if re.match(k, path):
                    if isinstance(data, str) and isinstance(v, dict):
                        return re.sub(v['regex'], v['value'], data)
                    return v
        return data


def save_config(data, output_config):
    with open(output_config, 'w', encoding='utf8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)


if __name__ == '__main__':
    main()
