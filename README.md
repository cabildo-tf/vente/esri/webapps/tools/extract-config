# Extract config
[![pipeline status](https://gitlab.com/cabildo-tf/vente/esri/webapps/tools/extract-config/badges/master/pipeline.svg)](https://gitlab.com/cabildo-tf/vente/esri/webapps/tools/extract-config/-/commits/master)
[![coverage report](https://gitlab.com/cabildo-tf/vente/esri/webapps/tools/extract-config/badges/master/coverage.svg)](https://gitlab.com/cabildo-tf/vente/esri/webapps/tools/extract-config/-/commits/master)

Unifica los ficheros de configuración ArcGIS Web AppBuilder, pudiendo además reemplazar parámetros
dentro del fichero por otros valores.

## Configuración para reemplazo
Para definir los reemplazos de parámetros dentro del fichero de configuración, se puede especificar de 2 maneras.

* Si se desea reemplazar el valor completo, se usa el PATH del item como clave (por ejemplo: 'R:map:itemId') y como valor,
el valor por el que se desea reemplazar.

* Si se desea reemplazar parte de la cadena, se usa de nuevo el PATH del item como clave y luego 2 entradas más una con
la expresión regular (regex) que caza con la cadena a reemplzar y otro con el valor (value) por el que sustituir el match.

```yaml
---
R:map:itemId: ${MAP_ID}
R:.*:url:
  regex: vente-pre.tenerife.es
  value: ${HOST_PORTAL}
R:portalUrl:
  regex: vente-pre.tenerife.es
  value: ${HOST_PORTAL}
R:map:portalUrl:
  regex: vente-pre.tenerife.es
  value: ${HOST_PORTAL}
R:title: ${TITLE}
R:subtitle: ${SUBTITLE}
R:logoLink: ${LOGO_LINK}
```
